﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MatchState
{
    PREMATCH,
    SELECT_UNIT,
    SELECT_ACTION,
    SELECT_TARGET,
    ANIMATING,
    POSTMATCH
}

public class MatchController : MonoBehaviour
{
    #region Static Fields
    static MatchController _instance;
    public static MatchController Instance { get { return _instance; } }
    static MatchUIController mui { get { return MatchUIController.Instance; } }

    static CameraController cam { get { return CameraController.Instance; } }
    #endregion


    #region Member Fields
    /// <summary>
    /// Unit currently selected for an action
    /// </summary>
    public Unit selectedUnit;

    /// <summary>
    /// The current skill that is trying to be cast
    /// </summary>
    public UnitSkillData selectedUnitSkill;

    /// <summary>
    /// Unit who is being hovered-over (preview)
    /// </summary>
    [SerializeField]
    Unit previewUnit;

    /// <summary>
    /// Whose turn is it?
    /// </summary>
    [Header("0 = Team A, 1 = Team B")]
    public int playerTurn = 1;

    [HideInInspector]
    public Board board;

    [SerializeField]
    MatchState _state;
    public MatchState State
    {
        get { return _state; }
        private set { _state = value; }
    }

    [SerializeField]
    List<Unit> teamOneUnits = new List<Unit>();
    [SerializeField]
    List<Unit> teamTwoUnits = new List<Unit>();
    #endregion


    #region Turn Control
    public void StartMatch()
    {
        if (State != MatchState.PREMATCH)
        {
            Debug.LogError("What.");
            return;
        }
        
        NextTurn();
    }

    public void PlaceUnitsOnBoard()
    {
        foreach (Unit u in teamOneUnits)
        {
            PlaceUnitOnCell(u);
        }
        foreach (Unit u in teamTwoUnits)
        {
            PlaceUnitOnCell(u);
        }
    }

    public void PlaceUnitOnCell(Unit u)
    {
        u.transform.position = u.UnitPosOnCell(board[u.location.row, u.location.col]);
    }

    public void ClickedCell(Cell c)
    {
        // do i have a unit selected
        if (selectedUnit != null)
        {
            // are they waiting to move?
            if (selectedUnit.selectingMove)
            {
                // do the damn move
                selectedUnit.selectingMove = false;
                selectedUnit.DoMarkedMovement(c.stack);
            }
            // are they waiting to attack?
            else
            {
                if (TryAttackCell(c))
                {
                    print("attack success");
                }
            }
        }
    }

    public void ClickedUnit(Unit u)
    {
        //if (selectedUnit && selectedUnit.HasActed) return;  // locks unit in if it's acted

        // Is it this unit's team's turn? (friendly)
        if (u.teamID == playerTurn)
        {
            if ((selectedUnit != null && !selectedUnit.HasActed))
                UnselectUnit();

            SelectUnit(u);
        }
        // Not their turn (enemy)
        else
        {
            if (State == MatchState.SELECT_TARGET)
            {
                // am i on a cell marked for attack?
                if (board[u.location].SelectedForAttack)
                {
                    TryAttackUnit(u);
                }
            }
        }
    }

    bool TryAttackCell(Cell c)
    {
        if (selectedUnit.selectingAttack)
        {
            //print("finna attack " + c.stack.location.ToString());
            ISkillEffect effect = selectedUnitSkill.skillEffect;

            // No target (target cell)
            if (selectedUnitSkill.targetMode == TargetMode.CELL)
            {
                effect.DoEffect(selectedUnit, c);
                return true;
            }
            // is there a targetable unit on this cell?
            else
            {
                Unit target = null;
                if (selectedUnitSkill.targetMode == TargetMode.UNIT)
                {
                    target = OtherEnemyOnCellStack(c.stack, Team.NONE);
                }
                if (target != null)
                {
                    effect.DoEffect(selectedUnit, target);
                    return true;
                }
            }
        }
        return false;
    }

    bool TryAttackUnit(Unit u)
    {
        if (selectedUnit.selectingAttack)
        {
            //print("finna attack " + c.stack.location.ToString());
            ISkillEffect effect = selectedUnitSkill.skillEffect;

            // No target (target cell)
            if (selectedUnitSkill.targetMode == TargetMode.CELL)
            {
                effect.DoEffect(selectedUnit, u.currentCellStack.Top());
                return true;
            }
            // is there a targetable unit on this cell?
            else
            {
                effect.DoEffect(selectedUnit, u);
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Go to the next turn, changing which player has control
    /// </summary>
    public void NextTurn()
    {
        playerTurn = playerTurn ^ 1;    // toggle next player
        State = MatchState.SELECT_UNIT;

        cam.FlipCam(playerTurn);

        UnselectUnit();
        UnmarkPossibleAttack();
        UnmarkPossibleMovement();
        ResetUnitMoves();
        ResetUnitAttacks();


        print("MATCH: It is now player " + (playerTurn + 1) + "'s turn");
    }

    void ResetUnitMoves()
    {
        foreach (Unit u in teamOneUnits)
        {
            u.movesLeft = u.movesPerTurn;
        }
        foreach (Unit u in teamTwoUnits)
        {
            u.movesLeft = u.movesPerTurn;
        }
    }

    void ResetUnitAttacks()
    {
        foreach (Unit u in teamOneUnits)
        {
            u.attacksLeft = u.attacksPerTurn;
        }
        foreach (Unit u in teamTwoUnits)
        {
            u.attacksLeft = u.attacksPerTurn;
        }
    }

    /// <summary>
    /// Reset the possible-movement indicators
    /// </summary>
    public void UnmarkPossibleMovement()
    {
        foreach (CellStack cs in FindObjectsOfType<CellStack>())
        {
            UnmarkMovementTile(cs);
            cs.ParentStack = null;
            cs.SelectedForMove = false;
        }
    }

    /// <summary>
    /// Reset the possible-attack indicators
    /// </summary>
    public void UnmarkPossibleAttack()
    {
        foreach (CellStack cs in FindObjectsOfType<CellStack>())
        {
            UnmarkAttackTile(cs);
            cs.ParentStack = null;
            cs.SelectedForAttack = false;
        }
    }
    #endregion


    #region Units/Unit Control
    /// <summary>
    /// Called when the user clicks on a skill button
    /// </summary>
    public void SkillPressed(int skillIndex)
    {
        UnitSkillData usd = selectedUnit.skillData[skillIndex];
        selectedUnitSkill = usd;
        print("Unit = " + selectedUnit.unitName + ", Skill = " + usd.skillName);
        MarkAttackable(selectedUnit, usd);
    }

    public void UnitDied(Unit u)
    {
        // TODO implement when unit dies
        // let the dying animation play

        // end game if base is dead
    }

    public void MarkMoveable(Unit u)
    {
        if (u.movesLeft > 0)
        {
            u.selectingMove = true;
            u.MarkPossibleAction(UnitAction.MOVE);
        }

        State = MatchState.SELECT_TARGET;
    }

    public void MarkAttackable(Unit u, UnitSkillData usd)
    {
        if (u.attacksLeft > 0)
        {
            u.selectingAttack = true;
            u.MarkPossibleAction(UnitAction.ATTACK, usd);
        }

        State = MatchState.SELECT_TARGET;
    }

    /// <summary>
    /// See if there is an enemy on this cell
    /// </summary>
    public Unit OtherEnemyOnCellStack(CellStack cs, int teamID)
    {
        if (teamID != 0)
        {
            foreach (Unit u in teamOneUnits)
            {
                if (u.location == cs.location)
                {
                    return u;
                }
            }
        }

        if (teamID != 1)
        {
            foreach (Unit u in teamTwoUnits)
            {
                if (u.location == cs.location)
                {
                    return u;
                }
            }
        }

        return null;
    }

    public Unit OtherEnemyOnCellStack(CellStack cs, Team teamID) { return OtherEnemyOnCellStack(cs, (int)teamID); }

    public bool MarkAttackTile(CellStack cs)
    {
        if (!cs.SelectedForAttack)
        {
            cs.Top().Tint(Color.red);
            cs.SelectedForAttack = true;
            return false;
        }

        return true;
    }

    /// <summary>
    /// Mark cell for movement if it isn't already
    /// </summary>
    public bool MarkMovementTile(CellStack cs)
    {
        if (!cs.SelectedForMove)
        {
            cs.Top().Tint(Color.blue);
            cs.SelectedForMove = true;
            return false;
        }

        return true;
    }

    /// <summary>
    /// Clear marking on cell
    /// </summary>
    public void UnmarkMovementTile(CellStack cs)
    {
        cs.Top().ClearTint();
    }

    /// <summary>
    /// Clear marking on cell
    /// </summary>
    public void UnmarkAttackTile(CellStack cs)
    {
        cs.Top().ClearTint();
    }

    /// <summary>
    /// Select specified Unit u
    /// </summary>
    public void SelectUnit(Unit u)
    {
        selectedUnit = null;
        selectedUnit = u;

        mui.DisplayInfo(u);
        State = MatchState.SELECT_ACTION;

        if (u.CanAct)
            mui.ShowActionPanel(u);

        cam.LookAt(u.transform.position);
    }

    /// <summary>
    /// Deselect the currently selected unit
    /// </summary>
    public void UnselectUnit()
    {
        if (selectedUnit != null)
        {
            selectedUnit.Unselect();
        }

        if (State == MatchState.SELECT_ACTION)
            State = MatchState.SELECT_UNIT;

        selectedUnit = null;
        mui.HideInfoCard();
        mui.HideActionPanel();
    }

    public void UnitDoneMoving(Unit u)
    {
        //print("Unit done moving");
        // done moving? show the action panel
        if (u.CanAct/*u.movesLeft > 0*/)
        {
            mui.ShowActionPanel(u);
            State = MatchState.SELECT_ACTION;
        }
        // otherwise, end turn
        else
        {
            UnselectUnit();
            NextTurn();
        }
    }

    public void UnitDoneAttacking(Unit u)
    {
        if (u.CanAct)
        {
            mui.ShowActionPanel(u);
        }
        // otherwise, end turn
        else
        {
            UnselectUnit();
            NextTurn();
        }
    }

    /// <summary>
    /// Try to get the 4 tiles surrounding a Position2D pos
    /// </summary>
    public List<CellStack> GetSurroundingCells(Position2D pos)
    {
        var r = new List<CellStack>();
        // Right
        if (pos.col + 1 < board.boardWidth)
        {
            r.Add(board[pos.row, pos.col + 1]);
        }
        // Top
        if (pos.row + 1 < board.boardHeight)
        {
            r.Add(board[pos.row + 1, pos.col]);
        }
        // Left
        if (pos.col - 1 >= 0)
        {
            r.Add(board[pos.row, pos.col - 1]);
        }
        // Bottom
        if (pos.row - 1 >= 0)
        {
            r.Add(board[pos.row - 1, pos.col]);
        }

        return r;
    }
    #endregion


    #region Unity Methods
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
        }
        _instance = this;
    }

    void Start()
    {
        board.board = board.GenerateBoard();
        PlaceUnitsOnBoard();
        StartMatch();
    }

    void Update()
    {
        _instance = this;
    }
    #endregion

    internal Color32 GetTeamColor(int teamID)
    {
        // TODO setup team color elsewhere
        // for now, hardcoded to yellow and purple
        return teamID == (int)Team.A ? Color.yellow : Color.magenta;
    }

    /// <summary>
    /// Called when the EndTurn button is pressed
    /// </summary>
    /// <returns></returns>
    public void EndTurnButton()
    {
        // cancel any current actions (move/ability)
        // TODO: implement
        // proceed to next turn
        NextTurn();
    }
}
