﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Util {
	// Lerp helpers
	public static class Ease {
		/// <summary>
		/// Eases out with sinerp
		/// </summary>
		/// <returns>The modified percentage</returns>
		/// <param name="t">percentage t (elapsedTime / animTime)</param>
		public static float Out(float t) {
			return Mathf.Sin(t * Mathf.PI * 0.5f);
		}
		
		/// <summary>
		/// Eases in with coserp
		/// </summary>
		/// <returns>The modified percentage</returns>
		/// <param name="t">percentage t (elapsedTime / animTime)</param>
		public static float In(float t) {
			return 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
		}
	
		public static float Exp(float t, int pow = 2) {
			return Mathf.Pow(t, pow);
		}
	
		public static float SmoothStep(float t) {
			return t * t * (3f - 2f*t);
		}
		
		public static float SmootherStep(float t) {
			return t*t*t * (t * (6f*t - 15f) + 10f);
		}
	}
	
	//
	public static bool IsHitbox(Collider col) {
		return col.gameObject.layer.CompareTo(10) == 0;
	}
	
	public static bool IsEnemy(Collider col) {
		return col.CompareTag("Enemy");
	}
	
	// Assumes it's already been pooled. Recycles an object after given timeout
	public static IEnumerator RecycleTimeout(GameObject obj, float timeout) {
		yield return new WaitForSeconds(timeout);
		//ObjectPool.Recycle(obj);  // TODO reimplement
	}
	// overload for xform
	public static IEnumerator RecycleTimeout(Transform obj, float timeout) {
		yield return new WaitForSeconds(timeout);
		//ObjectPool.Recycle(obj.gameObject);
	}
	
	public static void LockCursor() {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
	
	public static void UnlockCursor() {
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
}
