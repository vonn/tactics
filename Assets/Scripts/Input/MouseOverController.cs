﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MouseOverController : MonoBehaviour
{

    //public UIController uiController;

    int rayLength = 10000;

    [SerializeField]
    Transform previousTarget;
    [SerializeField]
    Transform target;

    static MatchController mc { get { return MatchController.Instance; } }

    void Update()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(camRay, out hit, rayLength, Layers.Clickable))
        {
            target = hit.collider.transform; //hit.collider.gameObject.GetComponent<PvPAvatar>();

            // if we have new target
            if (target == null || !target.Equals(previousTarget))
            {
                // unhighlight previous target
                if (previousTarget != null)
                {
                    //previousTarget.renderer.material = previousMaterial;
                    previousTarget.GetComponent<UnitVFX>().HighlightColor(Color.black);
                }

                // set previous
                previousTarget = target;

                // highlight this bad boy

                // set up outline color
                Color col = DetermineColor(target);

                // set the thing
                target.GetComponent<UnitVFX>().HighlightColor(col);
            }
        }
        else
        {
            if (previousTarget != null)
            {
                previousTarget.GetComponent<UnitVFX>().HighlightColor(Color.black);
            }

            target = null;
            previousTarget = null;
        }
    }

    public void LeftClickDown()
    {
        if (target != null)
        {
            //print("Clicked on " + target.name);

            var u = target.GetComponent<Unit>();
            var c = target.GetComponent<Cell>();
            // Clicked on a unit
            if (u != null)
            {
                mc.ClickedUnit(u);
            }
            // Clicked on a cell
            if (c != null)
            {
                mc.ClickedCell(c);
            }
        }
    }

    Color32 DetermineColor(Transform tar)
    {
        Unit u = tar.GetComponent<Unit>();
        if (u == null) return Color.magenta;

        if (u.teamID == mc.playerTurn)
        {
            return Color.blue;
        }
        else
        {
            return Color.red;
        }
    }
}