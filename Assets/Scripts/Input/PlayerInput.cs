﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    MouseOverController _moc;
    MatchController mc;

    void TakeInput()
    {
        // Right click on world
        if (Input.GetMouseButtonDown(1))
        {
            //
        }

        // Left click in world
        if (Input.GetMouseButtonDown(0))
        {
            if (mc.State == MatchState.SELECT_UNIT || mc.State == MatchState.SELECT_TARGET ||
                (mc.State == MatchState.SELECT_ACTION && !mc.selectedUnit.HasActed))
            {
                _moc.LeftClickDown();
            }
        }

        // TODO get keybd input for spells, etc.
    }


    #region Unity Methods
    void Update()
    {
        TakeInput();
    }

    void Start()
    {
        mc = MatchController.Instance;
    }
    #endregion
}
