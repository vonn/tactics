﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitActionPanel : MonoBehaviour
{
    [SerializeField]
    UnitActionButton[] actionButtons;

    [SerializeField]
    Button endTurnButton;

    MatchController mc { get { return MatchController.Instance; } }

    // Use this for initialization
    void Start()
    {
        actionButtons = GetComponentsInChildren<UnitActionButton>();

        endTurnButton.onClick.AddListener(mc.EndTurnButton);
    }

    /// <summary>
    /// Sets up the buttons to reflect the unit's skills
    /// </summary>
    /// <param name="u"></param>
    public void SetupButtons(Unit u)
    {
        // Enable skill buttons if they have sufficient AP
        for (int i = 1; i < actionButtons.Length; i++)
        {
            actionButtons[i].Text = u.skillData[i - 1].skillName;

            actionButtons[i].button.interactable = u.apCurrent >= u.skillData[i - 1].cost;
        }

        // Enable 'Move' button if they have moves left
        actionButtons[0].button.interactable = u.movesLeft > 0;
    }
}
