﻿using UnityEngine;
using System.Collections;

public class MatchUIController : MonoBehaviour
{
    #region Fields
    UnitInfoCard unitInfoCard;
    UnitActionPanel unitActionPanel;

    public static MatchUIController Instance;
    static MatchController mc { get { return MatchController.Instance; } }
    #endregion


    #region Unity Methods
    // Use this for initialization
    void Start()
    {
        Instance = this;
        unitInfoCard = GetComponentInChildren<UnitInfoCard>();
        unitActionPanel = GetComponentInChildren<UnitActionPanel>();
        HideActionPanel();
        HideInfoCard();
    }
    void Update()
    {
        Instance = this;
    }
    #endregion


    #region Showing/Hiding
    public void DisplayInfo(Unit u)
    {
        ShowInfoCard();
        unitInfoCard.ApTokens = u.apCurrent;
        unitInfoCard.HpRatio = (float)u.hpCurrent / (float)u.hpMax;
        unitInfoCard.Name = u.unitName;
        unitInfoCard.portaitBG.color = mc.GetTeamColor(u.teamID);
        unitInfoCard.portrait.sprite = u.unitImage;
    }

    public void HideInfoCard()
    {
        unitInfoCard.gameObject.SetActive(false);
    }
    public void ShowInfoCard()
    {
        unitInfoCard.gameObject.SetActive(true);
    }

    public void HideActionPanel()
    {
        unitActionPanel.gameObject.SetActive(false);
    }
    public void ShowActionPanel(Unit u)
    {
        unitActionPanel.SetupButtons(u);
        unitActionPanel.gameObject.SetActive(true);
    }
    #endregion


    #region Action Buttons
    public void ButtonMove()
    {
        if (mc.selectedUnit.movesLeft <= 0)
        {
            Debug.LogError("Shouldn't be able to click this.");
            return;
        }

        mc.MarkMoveable(mc.selectedUnit);
    }

    public void ButtonSkill1()
    {
        if (mc.selectedUnit.attacksLeft <= 0)
        {
            Debug.LogError("Shouldn't be able to click this.");
            return;
        }

        mc.SkillPressed(0);
    }

    public void ButtonSkill2()
    {
        if (mc.selectedUnit.attacksLeft <= 0)
        {
            Debug.LogError("Shouldn't be able to click this.");
            return;
        }

        mc.SkillPressed(1);
    }

    public void ButtonSkill3()
    {
        if (mc.selectedUnit.attacksLeft <= 0)
        {
            Debug.LogError("Shouldn't be able to click this.");
            return;
        }

        mc.SkillPressed(2);
    }
    #endregion
}
