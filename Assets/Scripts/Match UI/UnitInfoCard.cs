﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitInfoCard : MonoBehaviour
{
    #region Member Fields
    [SerializeField]
    private Text _name;
    public string Name
    {
        get { return _name.text; }
        set { _name.text = value; }
    }

    [SerializeField]
    private Image _hpBar;
    public float HpRatio
    {
        get { return _hpBar.fillAmount; }
        set { _hpBar.fillAmount = value; }
    }

    [SerializeField]
    private Image[] _apTokens;
    public int ApTokens { set { SetApTokens(value); } }

    public Image portaitBG;
    public Image portrait;
    #endregion


    #region AP Token Methods
    public void ClearApTokens()
    {
        foreach (Image tok in _apTokens)
        {
            DisableToken(tok);
        }
    }

    public void SetApTokens(int ap)
    {
        ClearApTokens();
        for (int i = 0; i < ap; i++)
        {
            EnableToken(_apTokens[i]);
        }
    }

    public void EnableToken(Image tok)
    {
        // TODO give real image.
        // for now, just set the color to yellow
        tok.color = Color.yellow;
    }
    public void DisableToken(Image tok)
    {
        // for now, just set the color to grey
        tok.color = Color.grey;
    }
    #endregion


    #region Unity Methods
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion
}
