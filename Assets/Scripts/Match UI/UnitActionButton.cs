﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitActionButton : MonoBehaviour
{
    public Button button;
    public string Text { set { SetButtonText(value); } }

    void Start()
    {
        button = GetComponent<Button>();
    }

    public void SetButtonText(string s)
    {
        var t = transform.GetChild(0).GetComponent<Text>();
        if (t == null)
        {
            print("panic");
            return;
        }

        t.text = s;
    }
}
