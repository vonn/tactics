﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    #region Member Fields
    Camera cam { get { return Camera.main; } }
    public static CameraController Instance;

    [SerializeField]
    bool manualOverride;

    /// <summary>
    /// Offset to what i'm lookin at
    /// </summary>
    [SerializeField]
    Vector3 offset = new Vector3(-12, 18, 12);
    public Vector3 target;

    [SerializeField]
    float _angle;
    float Angle
    {
        get { return _angle; }
        set { _angle = value % 360; }
    }
    [SerializeField]
    float radius;
    [SerializeField]
    float height;

    const float PLAYER_1_ANGLE = 315f;
    const float PLAYER_2_ANGLE = 135f;

    [SerializeField]
    float _zoom;
    float Zoom
    {
        get
        {
            return _zoom;
        }
        set
        {
            _zoom = value;

            if (orthoMode)
                _zoom = Mathf.Clamp(_zoom, MAX_ZOOM_ORTHO, MIN_ZOOM_ORTHO);
            else
                _zoom = Mathf.Clamp(_zoom, MAX_ZOOM_PERSPECTIVE, MIN_ZOOM_PERSPECTIVE);
        }
    }

    float MAX_ZOOM_ORTHO = 1;
    float MIN_ZOOM_ORTHO = 7;
    float MAX_ZOOM_PERSPECTIVE = 1;
    float MIN_ZOOM_PERSPECTIVE = 7;

    [SerializeField]
    float ScrollSpeed = 20f;
    [SerializeField]
    float ScrollEdge = 0.01f;

    bool orthoMode = false;

    [SerializeField]
    float animTime = 1.0f;
    #endregion


    #region Unity Methods
    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        Instance = this;

        HandleInput();
        orthoMode = cam.orthographic;

        if (orthoMode)
            cam.orthographicSize = Zoom;
        else
            radius = Zoom;

        // short circuit the automatic positioning
        if (manualOverride) return;

        float radians = Angle * Mathf.PI / 180f;
        Vector3 pos;
        pos.x = target.x + radius * Mathf.Cos(radians);
        pos.y = target.y + height;
        pos.z = target.z + radius * Mathf.Sin(radians);
        transform.position = pos;
        transform.LookAt(target);

    }
    #endregion


    public void LookAt(Position3D pos)
    {
        target.x = pos.col * BoardGenerator.deltaCol;
        target.y = pos.height * BoardGenerator.deltaHeight;
        target.z = pos.row * BoardGenerator.deltaRow;
    }


    void HandleInput()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            Zoom--;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            Zoom++;
        }
    }


    /// <summary>
    /// Flip the camera as so simulate the perspective of the other player
    /// </summary>
    public void FlipCam(int turn)
    {
        StartCoroutine(LerpToAngle(turn == 0 ? PLAYER_1_ANGLE : PLAYER_2_ANGLE));
    }

    IEnumerator LerpToAngle(float destAngle)
    {
        float startAngle = Angle;

        for (float elapsed = 0; elapsed < animTime; elapsed += Time.deltaTime)
        {
            Angle = Mathf.Lerp(startAngle, destAngle, Util.Ease.SmootherStep(elapsed / animTime));

            yield return null;
        }

        // snap
        Angle = destAngle;

        yield return null;
    }

    /// <summary>
    /// Tell the camera to snap to this position
    /// </summary>
    /// <param name="pos"></param>
    public void LookAt(Vector3 pos)
    {
        StartCoroutine(LerpToTarget(pos));
    }

    IEnumerator LerpToTarget(Vector3 destPos)
    {
        Vector3 startPos = target;

        for (float elapsed = 0; elapsed < animTime; elapsed += Time.deltaTime)
        {
            target = Vector3.Lerp(startPos, destPos, Util.Ease.SmootherStep(elapsed / animTime));

            yield return null;
        }

        // snap
        target = destPos;

        yield return null;
    }
}
