﻿using UnityEngine;
using System.Collections;


public class Cell : MonoBehaviour, ITargetable
{
    #region Member Fields
    public MeshRenderer Mesh;
    public Position3D location;
    MatchController _mc { get { return MatchController.Instance; } }
    public CellStack stack { get { return transform.parent.GetComponent<CellStack>(); } }
    //string _type;
    //public string Type { set { _type = value; } }   // TODO maybe enum instead
    #endregion


    public void Init(string type)
    {
        //this._type = type;
    }


    /// <summary>
    /// Tint a tile a given color
    /// </summary>
    /// <param name="color"></param>
    public void Tint(Color32 color)
    {
        Mesh.material.color = color;
    }
    /// <summary>
    /// Clear the tint on a tile to white
    /// </summary>
    public void ClearTint()
    {
        Tint(Color.white);
    }

    #region Unity Methods
    void Start()
    {
    }
    #endregion
}
