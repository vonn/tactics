﻿using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour
{
    public Color32 tintMove;    // for moveable tiles
    public Color32 tintHostile; // for hostile spells (attacks, debuffs, etc.)
    public Color32 tintFriendly;    // for friendly spells (heals, buffs, etc.)
    public Transform cellContainer;    // reference to all tiles
    public int boardWidth, boardHeight;
    [SerializeField]
    public CellStack[,] board;    // stacks of tiles
    [SerializeField]
    bool genBoard = true;

    #region Unity Methods
    // Use this for initialization
    void Start()
    {
        if (genBoard)
            board = GenerateBoard();

    }
    #endregion

    public CellStack[,] GenerateBoard()
    {
        return GetComponent<BoardGenerator>().GenerateBoard(boardWidth, boardHeight);
    }

    /// <summary>
    /// Whether a position p is on our board
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    bool OnBoard(Position2D p)
    {
        if (p.col < 0 || p.row < 0 || p.col >= boardWidth || p.row >= boardHeight)
            return false;
        // TODO additionally try to get the cell on the board and test it
        return true;
    }

    public CellStack this[int row, int col]
    {
        get { return board[row, col]; }
    }

    // overloads shortcuts for cellstack
    public CellStack this[Position2D pos]
    {
        get
        {
            return board[pos.row, pos.col];
        }
    }
}

