﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A container to hold a vertical stack of cells
/// </summary>
public class CellStack : MonoBehaviour
{
    #region Member Fields
    public static float heightDelta = .3f;
    public Position2D location;
    MatchController _mc { get { return MatchController.Instance; } }
    public List<Cell> cells = new List<Cell>();
    [SerializeField] bool _selectedForMove;
    public bool SelectedForMove
    {
        get
        {
            return _selectedForMove;
        }
        set
        {
            _selectedForMove = value;
            Top().GetComponent<Collider>().enabled = value;
        }
    }
    [SerializeField] bool _selectedForAttack;
    public bool SelectedForAttack
    {
        get
        {
            return _selectedForAttack;
        }
        set
        {
            _selectedForAttack = value;
            Top().GetComponent<Collider>().enabled = value;
        }
    }
    // Pathfinding
    public int moveCost;
    public int hScore;
    public int gScore;
    CellStack _parentStack;
    public CellStack ParentStack
    {
        get { return _parentStack; }
        set { _parentStack = value; }
    }
    [SerializeField]
    int fScore;
    #endregion


    /// <summary>
    /// Update the cells array to reflect what my current children are
    /// </summary>
    void UpdateCells()
    {
        cells.Clear();

        foreach (var c in GetComponentsInChildren<Cell>())
        {
            if (c != null && c.enabled) // avoids errors when deleting things
                cells.Add(c);
        }

        this.UpdatePositions();
    }

    /// <summary>
    /// Updates world positions of the cells in this stack
    /// </summary>
    void UpdatePositions()
    {
        Vector3 pos = this.transform.position;
        for (int i = 0; i < this.Count; i++)
        {
            this[i].location.height = i;
            this[i].transform.position = pos;
            pos.y += heightDelta;
        }
    }

    /// <summary>
    /// Get cell at height i
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public Cell GetCell(int i)
    {
        if (i < 0 || i >= transform.childCount)
        {
            Debug.LogError("Error: GetCell(" + i + ") - Index out of bounds.");
            return null;
        }

        return cells[i];
    }

    /// <summary>
    /// Remove the cell at the top of the stack and return it
    /// </summary>
    /// <returns></returns>
    public Cell Pop()
    {
        var topCell = this.Top();

        // null it's parent, so that the stack can update it's list properly
        topCell.transform.parent = null;

        this.UpdateCells();

        return topCell;
    }

    /// <summary>
    /// Push a cell c to the top of the stack
    /// </summary>
    /// <param name="c"></param>
    public void Push(Cell c)
    {
        cells.Add(c);
        // set the parent to stick it at the top
        c.transform.parent = this.transform;
        this.UpdateCells();
    }

    /// <summary>
    /// Inserts cell c in the stack at index i
    /// </summary>
    /// <param name="c"></param>
    /// <param name="index"></param>
    public void Insert(Cell c, int index)
    {
        Push(c);
        c.transform.SetSiblingIndex(index);
        this.UpdateCells();
    }

    /// <summary>
    /// Removes cell at index i
    /// </summary>
    /// <param name="i"></param>
    public void Remove(int i)
    {
        if (i < 0 || i >= cells.Count) return;

        // TODO objpool recycle this instead
        GameObject.Destroy(cells[i].gameObject);
        this.UpdateCells();
    }

    /// <summary>
    /// Returns the Cell at the top of the stack
    /// </summary>
    /// <returns></returns>
    public Cell Top()
    {
        return this[cells.Count - 1];
    }

    /// <summary>
    /// Returns the bottom of the stack
    /// </summary>
    /// <returns></returns>
    public Cell Bottom()
    {
        return this[0];
    }

    /// <summary>
    /// Overloaded brackets as a shortcut to GetCell(i)
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public Cell this[int i]
    {
        get
        {
            return GetCell(i);
        }
    }

    public int Count
    {
        get
        {
            return cells.Count;
        }
    }

    public void Init(int moveCost)
    {
        this.moveCost = moveCost;
        ParentStack = null;
        SelectedForMove = false;
    }

    #region Pathfinding
    public int GetGScore()
    {
        int parentCost = 0;
        if (_parentStack)
            parentCost = _parentStack.GetGScore();

        return parentCost + moveCost;
    }

    public int GetGScoreForAttack()
    {
        int parentCost = 0;
        if (_parentStack)
            parentCost = _parentStack.GetGScoreForAttack();

        return 1 + parentCost;
    }

    public int FScore()
    {
        return this.gScore + this.hScore;
    }
    #endregion

    public override string ToString()
    {
        return string.Format(@"pos=[{0}, {1}] g={2} h={3} f={4}",
            location.row, location.col, gScore, hScore, fScore);
    }


    #region Unity Methods
    void Start()
    {
        Init(1);
    }
    #endregion
}
