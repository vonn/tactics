﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used to generate game boards
/// </summary>
public class BoardGenerator : MonoBehaviour
{
    #region Member Fields
    [SerializeField]
    GameObject cellPrefab;
    [SerializeField]
    GameObject cellStackPrefab;
    [SerializeField]
    GameObject rowPrefab;

    public int minStackSize = 2;
    public int maxStackSize = 4;
    #endregion
    #region Static Fields
    public const int deltaCol = 1;
    public const int deltaRow = 1;
    public const float deltaHeight = .3f;
    #endregion

    #region Unity Methods
    void Start()
    {
        // TODO obj pool init
    }

    #endregion

    /// <summary>
    /// Generates board
    /// </summary>
    /// <param name="cols"></param>
    /// <param name="rows"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public CellStack[,] GenerateBoard(int cols, int rows, int? seed = null)
    {
        CellStack[,] board = new CellStack[rows, cols];
        Vector3 currentPos = transform.position;  // current position of the tile to generate
        if (seed.HasValue)
        {
            Random.seed = seed.Value;
        }

        // for each row
        for (int r = 0; r < rows; r++)
        {
            var row = (GameObject)GameObject.Instantiate(rowPrefab, currentPos, Quaternion.identity);
            row.transform.parent = transform;

            // for each column
            for (int c = 0; c < cols; c++)
            {
                // spawn new cell stack (TODO: obj pool spawn instead)
                var stackObj = (GameObject)Object.Instantiate(cellStackPrefab, currentPos, Quaternion.identity);
                CellStack cs = stackObj.GetComponent<CellStack>();

                cs.location.row = r;
                cs.location.col = c;
                //stackObj.transform.parent = parent;
                stackObj.transform.parent = row.transform;

                // TODO vary min/max
                GenerateStack(cs, minStackSize, maxStackSize);

                // put the tile in the board data structure
                board[r, c] = cs;

                // update col pos
                currentPos.x += deltaCol;
            }

            // reset x position; update row
            currentPos.x = 0;
            currentPos.z += deltaRow;
        }

        return board;
    }

    /// <summary>
    /// Generates a stack of cells in some range given
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    void GenerateStack(CellStack cs, int min, int max)
    {
        int n = Random.Range(min, max + 1);
        Vector3 pos = cs.transform.position;

        for (int i = 0; i < n; i++)
        {
            // TODO objpool
            var cellObj = (GameObject)Instantiate(cellPrefab, pos, Quaternion.identity);
            Cell c = cellObj.GetComponent<Cell>();
            // setup board pos
            c.location.col = cs.location.col;
            c.location.row = cs.location.row;
            c.location.height = i;

            // push onto stack
            cs.Push(c);
        }
    }
}