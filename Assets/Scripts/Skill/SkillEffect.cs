﻿using UnityEngine;
using System.Collections;


public interface ISkillEffect
{
    void DoEffect(Unit source, ITargetable target);
}
