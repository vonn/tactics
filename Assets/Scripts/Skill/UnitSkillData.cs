﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// </summary>
public class UnitSkillData : MonoBehaviour
{
    public Skill skillEnum;
    public string skillName = "";
    public int damage = 0;
    public int range = 0;
    public int cost = 0;
    public TargetPattern targetPattern;
    public TargetMode targetMode;
    public ISkillEffect skillEffect { get { return GetComponent<ISkillEffect>(); } }
    //public SkillEffect fx;    //TODO implement skillfx
}
