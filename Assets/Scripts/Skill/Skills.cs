﻿using UnityEngine;
using System.Collections;


public class Skills
{

    public static UnitSkillData Load(Skill s, Transform parent)
    {
        //Debug.Log("Loading skill s = " + s.ToString("g"));
        GameObject obj = (GameObject)GameObject.Instantiate(Resources.Load("SkillPrefabs/" + s.ToString("g")));
        obj.transform.SetParent(parent);
        UnitSkillData usd = obj.GetComponent<UnitSkillData>();

        if (usd == null)
            Debug.LogError("SKILL LOAD: Unknown or undefined skill, or problem loading skill '" + s.ToString("g") + "'");

        return usd;
    }
}


#region Enums for targeting, etc.
public enum TargetPattern
{
    RANGE, STOMP, CONE, LINE
}

/// <summary>
/// What can be targeted by this skill?
/// </summary>
public enum TargetMode
{
    UNIT, CELL
}
#endregion