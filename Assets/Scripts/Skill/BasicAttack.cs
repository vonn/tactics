﻿using UnityEngine;
using System.Collections;

public class BasicAttack : SkillObject, ISkillEffect
{
    UnitSkillData usd { get { return GetComponent<UnitSkillData>(); } }

    public void DoEffect(Unit source, ITargetable target)
    {
        Unit t = null;

        // Unit target
        if (target is Unit)
        {
            t = (Unit)target;
        }
        // Cell target
        else
        {
            print(target);
            Cell c = (Cell)target;
            CellStack cs = c.stack;
            t = mc.OtherEnemyOnCellStack(cs, Team.NONE);
        }

        if (t == null)
        {
            Debug.LogError("Error: Trying to attack an empty tile.");
            return;
        }

        // Here we should have a Unit to damage. Let's damage it!
        //print("Do the damn damage to " + t.ToString());
        source.attacksLeft--;
        source.apCurrent -= usd.cost;
        DoDamage(t);
        mc.UnitDoneAttacking(source);
    }

    void DoDamage(Unit u)
    {
        u.TakeDamage(usd.damage);
    }
}
