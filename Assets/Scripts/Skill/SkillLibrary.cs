﻿/// <summary>
/// A library of all the skills in the game.
/// Each skill should have a corresponding UnitSkillData script.
/// </summary>
public enum Skill
{
    BasicAttack,
    FireBreath,
    Meteor,
    DoubleTap
}