﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// A grid position (row, col)
/// </summary>
[System.Serializable]
public class Position2D
{
    public int row;
    public int col;

    public Position2D(int row, int col)
    {
        this.row = row;
        this.col = col;
    }

    #region overloading
    public static bool operator ==(Position2D a, Position2D b)
    {
        return (a.col == b.col && a.row == b.row);
    }

    public static bool operator !=(Position2D a, Position2D b)
    {
        return (a.col != b.col || a.row != b.row);
    }

    public override bool Equals(object obj)
    {
        return (Position2D)this == (Position2D)obj;
    }

    public override int GetHashCode()
    {
        //return base.GetHashCode();
        int hash = 13;
        hash = (hash * 7) + row.GetHashCode();
        hash = (hash * 7) + col.GetHashCode();

        return hash;
    }

    public override string ToString()
    {
        return String.Format(@"[{0}, {1}]", row, col);
    }
    #endregion
}

[System.Serializable]
public class Position3D : Position2D
{
    public int height;

    public Position3D(int row, int col, int height)
        : base(row, col)
    {
        this.height = height;
    }
}

public class Layers
{
    // Unity-provided layers
    public const int Default = 0;
    public const int TransparentFX = 1;
    public const int IgnoreRaycast = 2;
    public const int Water = 4;
    public const int UI = 5;
    // Custom layers
    public const int Cell = 8;
    public const int Unit = 9;

    public const int Clickable = 1 << Unit |
                                 1 << Cell;
}

