﻿using UnityEngine;
using System.Collections;

public class UnitVFX : MonoBehaviour
{
    // My renderer
    public Renderer matRenderer;

    public void Start()
    {
        matRenderer = GetComponentInChildren<Renderer>();
    }

    public void HighlightColor(Color32 col)
    {
        matRenderer.material.SetColor("_OutlineColor", col);
    }
}
