﻿using UnityEngine;
using System.Collections;

public class BasicUnit : Unit
{
    // Use this for initialization
    void Start()
    {
        Init();
    }

    public override void Init()
    {
        base.Init();
        this.moveRange = 3;
    }

    protected override void LoadResources()
    {
        // Load unit image
        unitImage = GameObject.Instantiate(Resources.Load<Sprite>("UnitPortraits/cat_mage_01"));
        if (unitImage == null) { Debug.LogError("Error loading image"); }

        // Further down the line, i should be loading the sprite for the unit also
    }


    protected override bool CanTraverse(Cell c)
    {
        return true;
    }


    #region Unit Tests
    [ContextMenu("Test Select")]
    public override void TestSelect()
    {
    }
    #endregion
}
