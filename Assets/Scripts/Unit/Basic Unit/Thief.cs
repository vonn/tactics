﻿using UnityEngine;
using System.Collections;

public class Thief : Unit
{
    // Use this for initialization
    void Start()
    {
        Init();
    }

    public override void Init()
    {
        this.moveRange = 3;

        skills = new Skill[] { 
            Skill.BasicAttack, 
            Skill.FireBreath, 
            Skill.DoubleTap 
        };

        base.Init();
    }

    protected override void LoadResources()
    {
        // Load unit image
        unitImage = GameObject.Instantiate(Resources.Load<Sprite>("UnitPortraits/cat_thief_01"));
        if (unitImage == null) { Debug.LogError("Error loading image"); }

        // Further down the line, i should be loading the sprite for the unit also
    }


    protected override bool CanTraverse(Cell c)
    {
        return true;
    }
}
