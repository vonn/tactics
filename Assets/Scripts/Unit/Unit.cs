﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;


public enum UnitAction
{
    MOVE = 0, ATTACK = 1
}

public enum Team
{
    NONE = -1, A = 0, B = 1
}


public class Unit : MonoBehaviour, ITargetable
{
    #region static fields
    public static int UNIT_SKILLS_MAX = 3;
    #endregion


    #region Member Fields
    /* ~Information~ */
    public string unitName = "Unit";
    public int hpMax;
    public int hpCurrent;
    public int teamID;
    public int apMax;
    public int apCurrent;
    /* ~Attributes~ */
    public Position2D location;
    public bool isRanged;
    [SerializeField]
    protected int moveRange;
    [SerializeField]
    public CellStack currentCellStack;
    protected CellStack CurrentCellStack
    {
        get { return currentCellStack; }
        set
        {
            currentCellStack = value;
            location = value.location;
        }
    }
    /* ~Skills~ */
    [SerializeField]
    protected Skill[] skills = new Skill[UNIT_SKILLS_MAX];
    public List<UnitSkillData> skillData = new List<UnitSkillData>(3);
    /* ~Turn States~ */
    public bool isMoving;
    public int movesPerTurn = 1;
    public int attacksPerTurn = 1;
    public int movesLeft = 1;
    public int attacksLeft = 1;
    public bool selectingMove;
    public bool selectingAttack;
    public bool CanAct { get { return movesLeft > 0 || attacksLeft > 0; } }
    //public bool HasNotActed { get { return movesLeft == movesPerTurn && attacksLeft == attacksPerTurn; } }
    public bool HasActed { get { return movesLeft != movesPerTurn || attacksLeft != attacksPerTurn; } }
    /* ~Pathfinding~ */
    List<CellStack> spOpenSteps = new List<CellStack>();
    List<CellStack> spClosedSteps = new List<CellStack>();
    List<CellStack> movementPath = new List<CellStack>();
    /* ~Display~ */
    [SerializeField]
    public Sprite unitImage;
    #endregion

    static MatchController mc { get { return MatchController.Instance; } }

    public virtual void Init()
    {
        LoadResources();
        LoadSkills();
    }

    /// <summary>
    /// Load prefabs for each unit skill
    /// </summary>
    void LoadSkills()
    {
        var folder = new GameObject("Skills");
        folder.transform.SetParent(this.transform);

        for (int s = 0; s < skills.Length; s++)
        {
            skillData.Add(Skills.Load(skills[s], folder.transform));
        }
    }

    public virtual void SetupGraphic()
    {
        // Implemented in sub-classes
    }

    /// <summary>
    /// Can this unit walk over Cell c?
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    protected virtual bool CanTraverse(Cell c)
    {
        return true;
    }

    void UpdateHpLabel()
    {
        // TODO actually implement hp bars on units
    }

    /// <summary>
    /// Deselect this unit
    /// </summary>
    public void Unselect()
    {
        selectingMove = false;
        selectingAttack = false;
        mc.UnmarkPossibleMovement();
        mc.UnmarkPossibleAttack();
    }

    public void MarkPossibleAction(UnitAction action, UnitSkillData skillData = null)
    {
        var mc = MatchController.Instance;
        // get tile where unit is standing
        CellStack startCell = mc.board[location.row, location.col]; // debug; does this work?
        spOpenSteps.Add(startCell);
        spClosedSteps.Add(startCell);
        // If we are selecting movement, mark the tiles
        if (action == UnitAction.MOVE)
        {
            //mc.MarkMovementTile(startCell);
        }

        int i = 0;
        do
        {
            CellStack current = spOpenSteps[i];
            // You get every 4 tiles surrounding the current tile
            List<CellStack> surroundingCells = mc.GetSurroundingCells(current.location);
            foreach (CellStack cs in surroundingCells)
            {
                // Already dealt with it? ignore it
                if (spClosedSteps.Contains(cs))
                    continue;
                if (action == UnitAction.MOVE)
                {
                    // If there is a unit on the tile and you are moving, ignore it. Can't move there.
                    if (mc.OtherEnemyOnCellStack(cs, (int)Team.NONE) != null)
                        continue;
                    // If you are moving and this unit can't walk over that tile type, ignore it
                    if (!CanTraverse(cs.Top()))
                        continue;
                }

                cs.ParentStack = null;
                cs.ParentStack = current;

                // If you can move over there, paint it
                if (action == UnitAction.MOVE)
                    mc.MarkMovementTile(cs);
                else if (action == UnitAction.ATTACK)
                    mc.MarkAttackTile(cs);

                // Check how much it costs to move to or attack that tile.
                if (action == UnitAction.MOVE)
                {
                    if (cs.GetGScore() > moveRange)
                        continue;
                }
                else if (action == UnitAction.ATTACK)
                {
                    // if it's a range attack, can this skill reach here?
                    if (skillData.targetPattern == TargetPattern.RANGE && cs.GetGScoreForAttack() > skillData.range)
                    {
                        continue;
                    }
                    // TODO: handle other attack patterns
                }
                spOpenSteps.Add(cs);
                spClosedSteps.Add(cs);
            }
            i++;
        } while (i < spOpenSteps.Count);
        spOpenSteps.Clear();
        spClosedSteps.Clear();
    }


    #region Pathfinding
    void InsertOrderedInOpenSteps(CellStack cs)
    {
        // Compute the step's F score
        int tileFScore = cs.FScore();
        int count = spOpenSteps.Count;
        // This will be the index at which we will insert the step
        int i = 0;
        for (; i < count; i++)
        {
            // If the step's F score is lower or equals to the step at index i
            if (tileFScore <= spOpenSteps[i].FScore())
            {
                // Then you found the index at which you have to insert the new step
                // Basically you want the list sorted by F score
                break;
            }
        }
        // Insert the new step at the determined index to preserve the F score ordering
        spOpenSteps.Insert(i, cs);
    }

    int ComputeHScoreFromCoord(Position2D from, Position2D to)
    {
        // Here you use the Manhattan method, which calculates the total number of steps moved horizontally and vertically to reach the
        // final desired step from the current step, ignoring any obstacles that may be in the way
        return Mathf.Abs(to.row - from.row) + Mathf.Abs(to.col - from.col);
    }

    int CostToMoveFromTile(CellStack from, CellStack to)
    {
        // Because you can't move diagonally and because terrain is just walkable or unwalkable the cost is always the same.
        // But it has to be different if you can move diagonally and/or if there are swamps, hills, etc...
        return 1;
    }

    void ConstructPathAndStartAnimationFromStep(CellStack cs)
    {
        movementPath.Clear();

        // Repeat until there are no more parents
        do
        {
            // Don't add the last step which is the start position (remember you go backward, so the last one is the origin position ;-)
            if (cs.ParentStack != null)
            {
                // Always insert at index 0 to reverse the path
                movementPath.Insert(0, cs);
            }
            // Go backward
            cs = cs.ParentStack;
        } while (cs != null);

        PopStepAndAnimate();
    }

    void PopStepAndAnimate()
    {
        // Check if there remain path steps to go through
        if (movementPath.Count == 0)
        {
            isMoving = false;
            mc.UnmarkPossibleMovement();

            //bool enemiesAreInRange = false;
            // You'll determine later if there is a nearby enemy to attack.
            mc.UnitDoneMoving(this);

            return;
        }

        // Get the next step to move toward
        CellStack cs = movementPath[0];

        // Prepare the action and the callback
        StartCoroutine(DoTheDamnMove(cs.location, .4f, PopStepAndAnimate));
        // set the method itself as the callback
        // Remove the step
        movementPath.RemoveAt(0);
        // Play actions
    }

    IEnumerator DoTheDamnMove(Position2D pos, float animTime, UnityAction postMove)
    {
        CellStack destCell = mc.board[pos.row, pos.col];
        Vector3 destPos = UnitPosOnCell(destCell);
        Vector3 startPos = transform.position;

        for (float elapsed = 0; elapsed < animTime; elapsed += Time.deltaTime)
        {
            transform.position = Vector3.Lerp(startPos, destPos, Util.Ease.SmootherStep(elapsed / animTime));

            yield return null;
        }
        // now the unit should be at the new tile
        location = pos;

        postMove();
    }

    public void DoMarkedMovement(CellStack target)
    {
        if (isMoving)
            return;
        isMoving = true;
        movesLeft--;
        MatchController mc = MatchController.Instance;
        CellStack startTile = mc.board[location.row, location.col];
        currentCellStack = startTile;
        InsertOrderedInOpenSteps(currentCellStack);

        do
        {
            CellStack _currentTile = spOpenSteps[0];
            Position2D _currentTileCoord = _currentTile.Top().location;

            spClosedSteps.Add(_currentTile);
            spOpenSteps.RemoveAt(0);

            // If the currentStep is the desired tile coordinate, you are done!
            if (_currentTile.location == target.location)
            {
                ConstructPathAndStartAnimationFromStep(_currentTile);
                // Set to nil to release unused memory
                spOpenSteps.Clear();
                // Set to nil to release unused memory
                spClosedSteps.Clear();
                break;
            }
            List<CellStack> tiles = mc.GetSurroundingCells(_currentTileCoord);

            foreach (CellStack tileValue in tiles)
            {
                Position2D tileCoord = tileValue.location;
                CellStack _neighbourTile = mc.board[tileCoord.row, tileCoord.col];

                if (spClosedSteps.Contains(_neighbourTile))
                {
                    continue;
                }
                if (mc.OtherEnemyOnCellStack(_neighbourTile, teamID))
                {
                    // Ignore it
                    continue;
                }
                if (!CanTraverse(_neighbourTile.Top()))
                {
                    // Ignore it
                    continue;
                }

                int moveCost = CostToMoveFromTile(_currentTile, _neighbourTile);
                int index = spOpenSteps.IndexOf(_neighbourTile);
                if (index == -1)
                {
                    _neighbourTile.ParentStack = null;
                    _neighbourTile.ParentStack = _currentTile;
                    _neighbourTile.gScore = _currentTile.gScore + moveCost;
                    _neighbourTile.hScore = ComputeHScoreFromCoord(_neighbourTile.location, target.location);
                    InsertOrderedInOpenSteps(_neighbourTile);
                }
                else
                {
                    // To retrieve the old one (which has its scores already computed ;-)
                    _neighbourTile = spOpenSteps[index];
                    // Check to see if the G score for that step is lower if you use the current step to get there
                    if ((_currentTile.gScore + moveCost) < _neighbourTile.gScore)
                    {
                        // The G score is equal to the parent G score + the cost to move from the parent to it
                        _neighbourTile.gScore = _currentTile.gScore + moveCost;
                        // Now you can remove it from the list without being afraid that it can't be released
                        spOpenSteps.RemoveAt(index);
                        // Re-insert it with the function, which is preserving the list ordered by F score
                        InsertOrderedInOpenSteps(_neighbourTile);
                    }
                }
            }
        } while (spOpenSteps.Count > 0);
    }
    #endregion


    #region Unity Methods

    #endregion

    public override string ToString()
    {
        return string.Format(@"Unit: {0} ({3}) @ pos=[{1},{2}]",
            unitName, location.row, location.col, name);
    }

    // stringy method to determine where the unit should stand on the cell
    public Vector3 UnitPosOnCell(CellStack cs)
    {
        // find out the 3d position first
        //Position3D pos3d = cs.Top().location;
        Vector3 topPos = cs.Top().transform.position;
        var unitBounds = GetComponent<BoxCollider>().bounds;
        var cellBounds = cs.Top().GetComponent<BoxCollider>().bounds;
        float topOfCell = cellBounds.max.y;
        float unitOriginToBottomOfCollider = transform.position.y - unitBounds.min.y;
        float yPos = topOfCell + unitOriginToBottomOfCollider;

        return new Vector3(topPos.x, yPos, topPos.z);
    }

    #region Unit Tests
    [AddComponentMenu("TestThing")]
    public virtual void TestSelect()
    {

    }
    #endregion


    #region Loading a unit
    /// <summary>
    /// Load the resources for a unit: sprites, sounds, etc.
    /// </summary>
    protected virtual void LoadResources()
    {
        // Implemented in child classes
    }
    #endregion


    #region Life and Death
    public void TakeDamage(int damage)
    {
        hpCurrent -= damage;

        if (hpCurrent <= 0)
        {
            mc.UnitDied(this);
        }
    }
    #endregion
}
